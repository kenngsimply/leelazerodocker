# Leela Zero dockerfile
#
# docker build --no-cache -t leelazeronetworkless:latest -f leelazero_dockerfile_networkless.txt .
#

FROM ubuntu
RUN apt update && apt install -y git wget unzip make cmake g++
RUN apt install -y libboost-dev libboost-program-options-dev libboost-filesystem-dev opencl-headers ocl-icd-libopencl1 ocl-icd-opencl-dev zlib1g-dev
RUN git clone https://github.com/gcp/leela-zero
RUN cd /leela-zero && git submodule update --init --recursive
RUN cd /leela-zero && mkdir build
RUN cd /leela-zero/build && CXX=g++ CC=gcc cmake -DUSE_CPU_ONLY=1 ..
RUN cd /leela-zero/build && cmake --build . --target leelaz